package in.iitb.web.rest;

import com.codahale.metrics.annotation.Timed;
import in.iitb.domain.Lvcategory;
import in.iitb.repository.LvcategoryRepository;
import in.iitb.repository.search.LvcategorySearchRepository;
import in.iitb.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Lvcategory.
 */
@RestController
@RequestMapping("/api")
public class LvcategoryResource {

    private final Logger log = LoggerFactory.getLogger(LvcategoryResource.class);
        
    @Inject
    private LvcategoryRepository lvcategoryRepository;
    
    @Inject
    private LvcategorySearchRepository lvcategorySearchRepository;
    
    /**
     * POST  /lvcategorys -> Create a new lvcategory.
     */
    @RequestMapping(value = "/lvcategorys",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Lvcategory> createLvcategory(@Valid @RequestBody Lvcategory lvcategory) throws URISyntaxException {
        log.debug("REST request to save Lvcategory : {}", lvcategory);
        if (lvcategory.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("lvcategory", "idexists", "A new lvcategory cannot already have an ID")).body(null);
        }
        Lvcategory result = lvcategoryRepository.save(lvcategory);
        lvcategorySearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/lvcategorys/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("lvcategory", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /lvcategorys -> Updates an existing lvcategory.
     */
    @RequestMapping(value = "/lvcategorys",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Lvcategory> updateLvcategory(@Valid @RequestBody Lvcategory lvcategory) throws URISyntaxException {
        log.debug("REST request to update Lvcategory : {}", lvcategory);
        if (lvcategory.getId() == null) {
            return createLvcategory(lvcategory);
        }
        Lvcategory result = lvcategoryRepository.save(lvcategory);
        lvcategorySearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("lvcategory", lvcategory.getId().toString()))
            .body(result);
    }

    /**
     * GET  /lvcategorys -> get all the lvcategorys.
     */
    @RequestMapping(value = "/lvcategorys",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Lvcategory> getAllLvcategorys() {
        log.debug("REST request to get all Lvcategorys");
        return lvcategoryRepository.findAll();
            }

    /**
     * GET  /lvcategorys/:id -> get the "id" lvcategory.
     */
    @RequestMapping(value = "/lvcategorys/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Lvcategory> getLvcategory(@PathVariable Long id) {
        log.debug("REST request to get Lvcategory : {}", id);
        Lvcategory lvcategory = lvcategoryRepository.findOne(id);
        return Optional.ofNullable(lvcategory)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /lvcategorys/:id -> delete the "id" lvcategory.
     */
    @RequestMapping(value = "/lvcategorys/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteLvcategory(@PathVariable Long id) {
        log.debug("REST request to delete Lvcategory : {}", id);
        lvcategoryRepository.delete(id);
        lvcategorySearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("lvcategory", id.toString())).build();
    }

    /**
     * SEARCH  /_search/lvcategorys/:query -> search for the lvcategory corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/lvcategorys/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Lvcategory> searchLvcategorys(@PathVariable String query) {
        log.debug("REST request to search Lvcategorys for query {}", query);
        return StreamSupport
            .stream(lvcategorySearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}
