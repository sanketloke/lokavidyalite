package in.iitb.web.rest;

import com.codahale.metrics.annotation.Timed;
import in.iitb.domain.ExternalVideo;
import in.iitb.repository.ExternalVideoRepository;
import in.iitb.repository.search.ExternalVideoSearchRepository;
import in.iitb.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing ExternalVideo.
 */
@RestController
@RequestMapping("/api")
public class ExternalVideoResource {

    private final Logger log = LoggerFactory.getLogger(ExternalVideoResource.class);
        
    @Inject
    private ExternalVideoRepository externalVideoRepository;
    
    @Inject
    private ExternalVideoSearchRepository externalVideoSearchRepository;
    
    /**
     * POST  /externalVideos -> Create a new externalVideo.
     */
    @RequestMapping(value = "/externalVideos",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ExternalVideo> createExternalVideo(@Valid @RequestBody ExternalVideo externalVideo) throws URISyntaxException {
        log.debug("REST request to save ExternalVideo : {}", externalVideo);
        if (externalVideo.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("externalVideo", "idexists", "A new externalVideo cannot already have an ID")).body(null);
        }
        ExternalVideo result = externalVideoRepository.save(externalVideo);
        externalVideoSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/externalVideos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("externalVideo", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /externalVideos -> Updates an existing externalVideo.
     */
    @RequestMapping(value = "/externalVideos",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ExternalVideo> updateExternalVideo(@Valid @RequestBody ExternalVideo externalVideo) throws URISyntaxException {
        log.debug("REST request to update ExternalVideo : {}", externalVideo);
        if (externalVideo.getId() == null) {
            return createExternalVideo(externalVideo);
        }
        ExternalVideo result = externalVideoRepository.save(externalVideo);
        externalVideoSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("externalVideo", externalVideo.getId().toString()))
            .body(result);
    }

    /**
     * GET  /externalVideos -> get all the externalVideos.
     */
    @RequestMapping(value = "/externalVideos",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<ExternalVideo> getAllExternalVideos(@RequestParam(required = false) String filter) {
        if ("parenttutorial-is-null".equals(filter)) {
            log.debug("REST request to get all ExternalVideos where parentTutorial is null");
            return StreamSupport
                .stream(externalVideoRepository.findAll().spliterator(), false)
                .filter(externalVideo -> externalVideo.getParentTutorial() == null)
                .collect(Collectors.toList());
        }
        log.debug("REST request to get all ExternalVideos");
        return externalVideoRepository.findAll();
            }

    /**
     * GET  /externalVideos/:id -> get the "id" externalVideo.
     */
    @RequestMapping(value = "/externalVideos/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ExternalVideo> getExternalVideo(@PathVariable Long id) {
        log.debug("REST request to get ExternalVideo : {}", id);
        ExternalVideo externalVideo = externalVideoRepository.findOne(id);
        return Optional.ofNullable(externalVideo)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /externalVideos/:id -> delete the "id" externalVideo.
     */
    @RequestMapping(value = "/externalVideos/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteExternalVideo(@PathVariable Long id) {
        log.debug("REST request to delete ExternalVideo : {}", id);
        externalVideoRepository.delete(id);
        externalVideoSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("externalVideo", id.toString())).build();
    }

    /**
     * SEARCH  /_search/externalVideos/:query -> search for the externalVideo corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/externalVideos/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<ExternalVideo> searchExternalVideos(@PathVariable String query) {
        log.debug("REST request to search ExternalVideos for query {}", query);
        return StreamSupport
            .stream(externalVideoSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}
