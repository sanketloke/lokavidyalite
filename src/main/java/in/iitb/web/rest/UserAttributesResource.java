package in.iitb.web.rest;

import com.codahale.metrics.annotation.Timed;
import in.iitb.domain.UserAttributes;
import in.iitb.repository.UserAttributesRepository;
import in.iitb.repository.search.UserAttributesSearchRepository;
import in.iitb.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing UserAttributes.
 */
@RestController
@RequestMapping("/api")
public class UserAttributesResource {

    private final Logger log = LoggerFactory.getLogger(UserAttributesResource.class);
        
    @Inject
    private UserAttributesRepository userAttributesRepository;
    
    @Inject
    private UserAttributesSearchRepository userAttributesSearchRepository;
    
    /**
     * POST  /userAttributess -> Create a new userAttributes.
     */
    @RequestMapping(value = "/userAttributess",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<UserAttributes> createUserAttributes(@Valid @RequestBody UserAttributes userAttributes) throws URISyntaxException {
        log.debug("REST request to save UserAttributes : {}", userAttributes);
        if (userAttributes.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("userAttributes", "idexists", "A new userAttributes cannot already have an ID")).body(null);
        }
        UserAttributes result = userAttributesRepository.save(userAttributes);
        userAttributesSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/userAttributess/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("userAttributes", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /userAttributess -> Updates an existing userAttributes.
     */
    @RequestMapping(value = "/userAttributess",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<UserAttributes> updateUserAttributes(@Valid @RequestBody UserAttributes userAttributes) throws URISyntaxException {
        log.debug("REST request to update UserAttributes : {}", userAttributes);
        if (userAttributes.getId() == null) {
            return createUserAttributes(userAttributes);
        }
        UserAttributes result = userAttributesRepository.save(userAttributes);
        userAttributesSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("userAttributes", userAttributes.getId().toString()))
            .body(result);
    }

    /**
     * GET  /userAttributess -> get all the userAttributess.
     */
    @RequestMapping(value = "/userAttributess",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<UserAttributes> getAllUserAttributess() {
        log.debug("REST request to get all UserAttributess");
        return userAttributesRepository.findAll();
            }

    /**
     * GET  /userAttributess/:id -> get the "id" userAttributes.
     */
    @RequestMapping(value = "/userAttributess/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<UserAttributes> getUserAttributes(@PathVariable Long id) {
        log.debug("REST request to get UserAttributes : {}", id);
        UserAttributes userAttributes = userAttributesRepository.findOne(id);
        return Optional.ofNullable(userAttributes)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /userAttributess/:id -> delete the "id" userAttributes.
     */
    @RequestMapping(value = "/userAttributess/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteUserAttributes(@PathVariable Long id) {
        log.debug("REST request to delete UserAttributes : {}", id);
        userAttributesRepository.delete(id);
        userAttributesSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("userAttributes", id.toString())).build();
    }

    /**
     * SEARCH  /_search/userAttributess/:query -> search for the userAttributes corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/userAttributess/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<UserAttributes> searchUserAttributess(@PathVariable String query) {
        log.debug("REST request to search UserAttributess for query {}", query);
        return StreamSupport
            .stream(userAttributesSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}
