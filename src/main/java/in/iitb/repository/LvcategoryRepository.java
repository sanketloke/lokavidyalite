package in.iitb.repository;

import in.iitb.domain.Lvcategory;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Lvcategory entity.
 */
public interface LvcategoryRepository extends JpaRepository<Lvcategory,Long> {

}
