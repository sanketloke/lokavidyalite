package in.iitb.repository;

import in.iitb.domain.Tutorial;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Spring Data JPA repository for the Tutorial entity.
 */
public interface TutorialRepository extends JpaRepository<Tutorial,Long> {

    @Query("select distinct tutorial from Tutorial tutorial left join fetch tutorial.playlists left join fetch tutorial.tags")
    List<Tutorial> findAllWithEagerRelationships();

    @Query("select tutorial from Tutorial tutorial left join fetch tutorial.playlists left join fetch tutorial.tags where tutorial.id =:id")
    Tutorial findOneWithEagerRelationships(@Param("id") Long id);

}
