package in.iitb.repository;

import in.iitb.domain.UserAttributes;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the UserAttributes entity.
 */
public interface UserAttributesRepository extends JpaRepository<UserAttributes,Long> {

}
