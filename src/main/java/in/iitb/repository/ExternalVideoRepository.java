package in.iitb.repository;

import in.iitb.domain.ExternalVideo;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the ExternalVideo entity.
 */
public interface ExternalVideoRepository extends JpaRepository<ExternalVideo,Long> {

}
