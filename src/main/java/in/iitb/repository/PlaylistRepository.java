package in.iitb.repository;

import in.iitb.domain.Playlist;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Playlist entity.
 */
public interface PlaylistRepository extends JpaRepository<Playlist,Long> {

}
