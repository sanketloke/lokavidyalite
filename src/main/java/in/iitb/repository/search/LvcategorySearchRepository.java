package in.iitb.repository.search;

import in.iitb.domain.Lvcategory;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the Lvcategory entity.
 */
public interface LvcategorySearchRepository extends ElasticsearchRepository<Lvcategory, Long> {
}
