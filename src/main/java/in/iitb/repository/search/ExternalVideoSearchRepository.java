package in.iitb.repository.search;

import in.iitb.domain.ExternalVideo;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the ExternalVideo entity.
 */
public interface ExternalVideoSearchRepository extends ElasticsearchRepository<ExternalVideo, Long> {
}
