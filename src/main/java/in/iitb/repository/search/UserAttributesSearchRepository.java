package in.iitb.repository.search;

import in.iitb.domain.UserAttributes;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the UserAttributes entity.
 */
public interface UserAttributesSearchRepository extends ElasticsearchRepository<UserAttributes, Long> {
}
