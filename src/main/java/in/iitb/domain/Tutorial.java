package in.iitb.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import java.time.LocalDate;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Tutorial.
 */
@Entity
@Table(name = "tutorial")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "tutorial")
public class Tutorial implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @NotNull
    @Column(name = "description", nullable = false)
    private String description;

    @Column(name = "upvote_count")
    private Long upvoteCount;

    @Column(name = "downvote_count")
    private Long downvoteCount;

    @Column(name = "created_at")
    private LocalDate createdAt;

    @Column(name = "updated_at")
    private LocalDate updatedAt;

    @Column(name = "author_name")
    private String authorName;

    @NotNull
    @Column(name = "video_url", nullable = false)
    private String videoUrl;

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "tutorial_playlist",
               joinColumns = @JoinColumn(name="tutorials_id", referencedColumnName="ID"),
               inverseJoinColumns = @JoinColumn(name="playlists_id", referencedColumnName="ID"))
    private Set<Playlist> playlists = new HashSet<>();

    @OneToOne
    private User user;

    @OneToOne
    private ExternalVideo externalVideo;

    @OneToOne
    private SegmentVideo segmentVideo;

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "tutorial_tag",
               joinColumns = @JoinColumn(name="tutorials_id", referencedColumnName="ID"),
               inverseJoinColumns = @JoinColumn(name="tags_id", referencedColumnName="ID"))
    private Set<Tag> tags = new HashSet<>();

    @OneToOne
    private Lvcategory lvcategory;

    @OneToMany(mappedBy = "baseTutorial")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Comment> comments = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getUpvoteCount() {
        return upvoteCount;
    }

    public void setUpvoteCount(Long upvoteCount) {
        this.upvoteCount = upvoteCount;
    }

    public Long getDownvoteCount() {
        return downvoteCount;
    }

    public void setDownvoteCount(Long downvoteCount) {
        this.downvoteCount = downvoteCount;
    }

    public LocalDate getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDate createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDate getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDate updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public Set<Playlist> getPlaylists() {
        return playlists;
    }

    public void setPlaylists(Set<Playlist> playlists) {
        this.playlists = playlists;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public ExternalVideo getExternalVideo() {
        return externalVideo;
    }

    public void setExternalVideo(ExternalVideo externalVideo) {
        this.externalVideo = externalVideo;
    }

    public SegmentVideo getSegmentVideo() {
        return segmentVideo;
    }

    public void setSegmentVideo(SegmentVideo segmentVideo) {
        this.segmentVideo = segmentVideo;
    }

    public Set<Tag> getTags() {
        return tags;
    }

    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

    public Lvcategory getLvcategory() {
        return lvcategory;
    }

    public void setLvcategory(Lvcategory lvcategory) {
        this.lvcategory = lvcategory;
    }

    public Set<Comment> getComments() {
        return comments;
    }

    public void setComments(Set<Comment> comments) {
        this.comments = comments;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Tutorial tutorial = (Tutorial) o;
        return Objects.equals(id, tutorial.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Tutorial{" +
            "id=" + id +
            ", name='" + name + "'" +
            ", description='" + description + "'" +
            ", upvoteCount='" + upvoteCount + "'" +
            ", downvoteCount='" + downvoteCount + "'" +
            ", createdAt='" + createdAt + "'" +
            ", updatedAt='" + updatedAt + "'" +
            ", authorName='" + authorName + "'" +
            ", videoUrl='" + videoUrl + "'" +
            '}';
    }
}
