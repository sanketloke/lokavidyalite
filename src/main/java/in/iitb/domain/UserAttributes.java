package in.iitb.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import java.time.LocalDate;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import in.iitb.domain.enumeration.Gender;

import in.iitb.domain.enumeration.Language;

/**
 * A UserAttributes.
 */
@Entity
@Table(name = "user_attributes")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "userattributes")
public class UserAttributes implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "request_token")
    private String requestToken;

    @Column(name = "access_token")
    private String accessToken;

    @Column(name = "expiration_dateof_token")
    private LocalDate expirationDateofToken;

    @Enumerated(EnumType.STRING)
    @Column(name = "gender")
    private Gender gender;

    @Enumerated(EnumType.STRING)
    @Column(name = "language")
    private Language language;

    @Column(name = "latent_ability_score")
    private Double latentAbilityScore;

    @Column(name = "created_at")
    private LocalDate createdAt;

    @Column(name = "updated_at")
    private LocalDate updatedAt;

    @NotNull
    @Column(name = "extra_boolean_settings", nullable = false)
    private Boolean extraBooleanSettings;

    @OneToOne
    private Image image;

    @OneToOne
    private Location location;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRequestToken() {
        return requestToken;
    }

    public void setRequestToken(String requestToken) {
        this.requestToken = requestToken;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public LocalDate getExpirationDateofToken() {
        return expirationDateofToken;
    }

    public void setExpirationDateofToken(LocalDate expirationDateofToken) {
        this.expirationDateofToken = expirationDateofToken;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public Double getLatentAbilityScore() {
        return latentAbilityScore;
    }

    public void setLatentAbilityScore(Double latentAbilityScore) {
        this.latentAbilityScore = latentAbilityScore;
    }

    public LocalDate getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDate createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDate getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDate updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Boolean getExtraBooleanSettings() {
        return extraBooleanSettings;
    }

    public void setExtraBooleanSettings(Boolean extraBooleanSettings) {
        this.extraBooleanSettings = extraBooleanSettings;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        UserAttributes userAttributes = (UserAttributes) o;
        return Objects.equals(id, userAttributes.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "UserAttributes{" +
            "id=" + id +
            ", requestToken='" + requestToken + "'" +
            ", accessToken='" + accessToken + "'" +
            ", expirationDateofToken='" + expirationDateofToken + "'" +
            ", gender='" + gender + "'" +
            ", language='" + language + "'" +
            ", latentAbilityScore='" + latentAbilityScore + "'" +
            ", createdAt='" + createdAt + "'" +
            ", updatedAt='" + updatedAt + "'" +
            ", extraBooleanSettings='" + extraBooleanSettings + "'" +
            '}';
    }
}
