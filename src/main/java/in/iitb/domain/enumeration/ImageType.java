package in.iitb.domain.enumeration;

/**
 * The ImageType enumeration.
 */
public enum ImageType {
    PROFILEPIC,INTERESTPIC,TUTORIALPIC,THUMBNAILPIC
}
