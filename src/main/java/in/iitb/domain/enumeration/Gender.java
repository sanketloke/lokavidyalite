package in.iitb.domain.enumeration;

/**
 * The Gender enumeration.
 */
public enum Gender {
    MALE,FEMALE
}
