package in.iitb.domain.enumeration;

/**
 * The Origin enumeration.
 */
public enum Origin {
    SYSTEM,HUMAN
}
