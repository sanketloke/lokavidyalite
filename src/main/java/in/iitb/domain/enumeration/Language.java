package in.iitb.domain.enumeration;

/**
 * The Language enumeration.
 */
public enum Language {
    ENGLISH,HINDI,MARATHI,ASSAMESE
}
