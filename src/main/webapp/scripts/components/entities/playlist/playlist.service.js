'use strict';

angular.module('lokavidyaApp')
    .factory('Playlist', function ($resource, DateUtils) {
        return $resource('api/playlists/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    data.createdAt = DateUtils.convertLocaleDateFromServer(data.createdAt);
                    data.updatedAt = DateUtils.convertLocaleDateFromServer(data.updatedAt);
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    data.createdAt = DateUtils.convertLocaleDateToServer(data.createdAt);
                    data.updatedAt = DateUtils.convertLocaleDateToServer(data.updatedAt);
                    return angular.toJson(data);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    data.createdAt = DateUtils.convertLocaleDateToServer(data.createdAt);
                    data.updatedAt = DateUtils.convertLocaleDateToServer(data.updatedAt);
                    return angular.toJson(data);
                }
            }
        });
    });
