'use strict';

angular.module('lokavidyaApp')
    .factory('PlaylistSearch', function ($resource) {
        return $resource('api/_search/playlists/:query', {}, {
            'query': { method: 'GET', isArray: true}
        });
    });
