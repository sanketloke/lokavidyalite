'use strict';

angular.module('lokavidyaApp')
    .factory('VoteSearch', function ($resource) {
        return $resource('api/_search/votes/:query', {}, {
            'query': { method: 'GET', isArray: true}
        });
    });
