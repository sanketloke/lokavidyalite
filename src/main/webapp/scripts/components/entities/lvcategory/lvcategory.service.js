'use strict';

angular.module('lokavidyaApp')
    .factory('Lvcategory', function ($resource, DateUtils) {
        return $resource('api/lvcategorys/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    });
