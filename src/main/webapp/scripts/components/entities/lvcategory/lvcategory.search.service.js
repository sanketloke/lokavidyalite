'use strict';

angular.module('lokavidyaApp')
    .factory('LvcategorySearch', function ($resource) {
        return $resource('api/_search/lvcategorys/:query', {}, {
            'query': { method: 'GET', isArray: true}
        });
    });
