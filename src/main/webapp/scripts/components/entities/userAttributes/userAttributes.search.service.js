'use strict';

angular.module('lokavidyaApp')
    .factory('UserAttributesSearch', function ($resource) {
        return $resource('api/_search/userAttributess/:query', {}, {
            'query': { method: 'GET', isArray: true}
        });
    });
