'use strict';

angular.module('lokavidyaApp')
    .factory('ExternalVideoSearch', function ($resource) {
        return $resource('api/_search/externalVideos/:query', {}, {
            'query': { method: 'GET', isArray: true}
        });
    });
