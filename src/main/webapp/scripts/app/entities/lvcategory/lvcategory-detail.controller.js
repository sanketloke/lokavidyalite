'use strict';

angular.module('lokavidyaApp')
    .controller('LvcategoryDetailController', function ($scope, $rootScope, $stateParams, entity, Lvcategory, Image) {
        $scope.lvcategory = entity;
        $scope.load = function (id) {
            Lvcategory.get({id: id}, function(result) {
                $scope.lvcategory = result;
            });
        };
        var unsubscribe = $rootScope.$on('lokavidyaApp:lvcategoryUpdate', function(event, result) {
            $scope.lvcategory = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
