'use strict';

angular.module('lokavidyaApp')
    .controller('LvcategoryController', function ($scope, $state, Lvcategory, LvcategorySearch) {

        $scope.lvcategorys = [];
        $scope.loadAll = function() {
            Lvcategory.query(function(result) {
               $scope.lvcategorys = result;
            });
        };
        $scope.loadAll();


        $scope.search = function () {
            LvcategorySearch.query({query: $scope.searchQuery}, function(result) {
                $scope.lvcategorys = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.lvcategory = {
                name: null,
                description: null,
                origin: null,
                parentCatId: null,
                levelFromRoot: null,
                id: null
            };
        };
    });
