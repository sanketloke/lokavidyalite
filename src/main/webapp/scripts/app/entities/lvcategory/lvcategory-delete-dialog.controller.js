'use strict';

angular.module('lokavidyaApp')
	.controller('LvcategoryDeleteController', function($scope, $uibModalInstance, entity, Lvcategory) {

        $scope.lvcategory = entity;
        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        $scope.confirmDelete = function (id) {
            Lvcategory.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        };

    });
