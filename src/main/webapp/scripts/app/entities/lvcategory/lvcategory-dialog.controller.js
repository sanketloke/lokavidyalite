'use strict';

angular.module('lokavidyaApp').controller('LvcategoryDialogController',
    ['$scope', '$stateParams', '$uibModalInstance', '$q', 'entity', 'Lvcategory', 'Image',
        function($scope, $stateParams, $uibModalInstance, $q, entity, Lvcategory, Image) {

        $scope.lvcategory = entity;
        $scope.images = Image.query({filter: 'lvcategory-is-null'});
        $q.all([$scope.lvcategory.$promise, $scope.images.$promise]).then(function() {
            if (!$scope.lvcategory.image || !$scope.lvcategory.image.id) {
                return $q.reject();
            }
            return Image.get({id : $scope.lvcategory.image.id}).$promise;
        }).then(function(image) {
            $scope.images.push(image);
        });
        $scope.load = function(id) {
            Lvcategory.get({id : id}, function(result) {
                $scope.lvcategory = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('lokavidyaApp:lvcategoryUpdate', result);
            $uibModalInstance.close(result);
            $scope.isSaving = false;
        };

        var onSaveError = function (result) {
            $scope.isSaving = false;
        };

        $scope.save = function () {
            $scope.isSaving = true;
            if ($scope.lvcategory.id != null) {
                Lvcategory.update($scope.lvcategory, onSaveSuccess, onSaveError);
            } else {
                Lvcategory.save($scope.lvcategory, onSaveSuccess, onSaveError);
            }
        };

        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
}]);
