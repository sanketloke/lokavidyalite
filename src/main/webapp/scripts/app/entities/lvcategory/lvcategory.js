'use strict';

angular.module('lokavidyaApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('lvcategory', {
                parent: 'entity',
                url: '/lvcategorys',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'Lvcategorys'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/lvcategory/lvcategorys.html',
                        controller: 'LvcategoryController'
                    }
                },
                resolve: {
                }
            })
            .state('lvcategory.detail', {
                parent: 'entity',
                url: '/lvcategory/{id}',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'Lvcategory'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/lvcategory/lvcategory-detail.html',
                        controller: 'LvcategoryDetailController'
                    }
                },
                resolve: {
                    entity: ['$stateParams', 'Lvcategory', function($stateParams, Lvcategory) {
                        return Lvcategory.get({id : $stateParams.id});
                    }]
                }
            })
            .state('lvcategory.new', {
                parent: 'lvcategory',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/lvcategory/lvcategory-dialog.html',
                        controller: 'LvcategoryDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {
                                    name: null,
                                    description: null,
                                    origin: null,
                                    parentCatId: null,
                                    levelFromRoot: null,
                                    id: null
                                };
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('lvcategory', null, { reload: true });
                    }, function() {
                        $state.go('lvcategory');
                    })
                }]
            })
            .state('lvcategory.edit', {
                parent: 'lvcategory',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/lvcategory/lvcategory-dialog.html',
                        controller: 'LvcategoryDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['Lvcategory', function(Lvcategory) {
                                return Lvcategory.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('lvcategory', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            })
            .state('lvcategory.delete', {
                parent: 'lvcategory',
                url: '/{id}/delete',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/lvcategory/lvcategory-delete-dialog.html',
                        controller: 'LvcategoryDeleteController',
                        size: 'md',
                        resolve: {
                            entity: ['Lvcategory', function(Lvcategory) {
                                return Lvcategory.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('lvcategory', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });
