'use strict';

angular.module('lokavidyaApp').controller('PlaylistDialogController',
    ['$scope', '$stateParams', '$uibModalInstance', 'entity', 'Playlist', 'Tutorial',
        function($scope, $stateParams, $uibModalInstance, entity, Playlist, Tutorial) {

        $scope.playlist = entity;
        $scope.tutorials = Tutorial.query();
        $scope.load = function(id) {
            Playlist.get({id : id}, function(result) {
                $scope.playlist = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('lokavidyaApp:playlistUpdate', result);
            $uibModalInstance.close(result);
            $scope.isSaving = false;
        };

        var onSaveError = function (result) {
            $scope.isSaving = false;
        };

        $scope.save = function () {
            $scope.isSaving = true;
            if ($scope.playlist.id != null) {
                Playlist.update($scope.playlist, onSaveSuccess, onSaveError);
            } else {
                Playlist.save($scope.playlist, onSaveSuccess, onSaveError);
            }
        };

        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        $scope.datePickerForCreatedAt = {};

        $scope.datePickerForCreatedAt.status = {
            opened: false
        };

        $scope.datePickerForCreatedAtOpen = function($event) {
            $scope.datePickerForCreatedAt.status.opened = true;
        };
        $scope.datePickerForUpdatedAt = {};

        $scope.datePickerForUpdatedAt.status = {
            opened: false
        };

        $scope.datePickerForUpdatedAtOpen = function($event) {
            $scope.datePickerForUpdatedAt.status.opened = true;
        };
}]);
