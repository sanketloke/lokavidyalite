'use strict';

angular.module('lokavidyaApp')
    .controller('PlaylistDetailController', function ($scope, $rootScope, $stateParams, entity, Playlist, Tutorial) {
        $scope.playlist = entity;
        $scope.load = function (id) {
            Playlist.get({id: id}, function(result) {
                $scope.playlist = result;
            });
        };
        var unsubscribe = $rootScope.$on('lokavidyaApp:playlistUpdate', function(event, result) {
            $scope.playlist = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
