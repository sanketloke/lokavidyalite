'use strict';

angular.module('lokavidyaApp')
    .controller('PlaylistController', function ($scope, $state, Playlist, PlaylistSearch) {

        $scope.playlists = [];
        $scope.loadAll = function() {
            Playlist.query(function(result) {
               $scope.playlists = result;
            });
        };
        $scope.loadAll();


        $scope.search = function () {
            PlaylistSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.playlists = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.playlist = {
                title: null,
                description: null,
                createdAt: null,
                updatedAt: null,
                id: null
            };
        };
    });
