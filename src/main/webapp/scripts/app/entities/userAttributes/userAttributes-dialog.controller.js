'use strict';

angular.module('lokavidyaApp').controller('UserAttributesDialogController',
    ['$scope', '$stateParams', '$uibModalInstance', '$q', 'entity', 'UserAttributes', 'Image', 'Location',
        function($scope, $stateParams, $uibModalInstance, $q, entity, UserAttributes, Image, Location) {

        $scope.userAttributes = entity;
        $scope.images = Image.query({filter: 'userattributes-is-null'});
        $q.all([$scope.userAttributes.$promise, $scope.images.$promise]).then(function() {
            if (!$scope.userAttributes.image || !$scope.userAttributes.image.id) {
                return $q.reject();
            }
            return Image.get({id : $scope.userAttributes.image.id}).$promise;
        }).then(function(image) {
            $scope.images.push(image);
        });
        $scope.locations = Location.query({filter: 'userattributes-is-null'});
        $q.all([$scope.userAttributes.$promise, $scope.locations.$promise]).then(function() {
            if (!$scope.userAttributes.location || !$scope.userAttributes.location.id) {
                return $q.reject();
            }
            return Location.get({id : $scope.userAttributes.location.id}).$promise;
        }).then(function(location) {
            $scope.locations.push(location);
        });
        $scope.load = function(id) {
            UserAttributes.get({id : id}, function(result) {
                $scope.userAttributes = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('lokavidyaApp:userAttributesUpdate', result);
            $uibModalInstance.close(result);
            $scope.isSaving = false;
        };

        var onSaveError = function (result) {
            $scope.isSaving = false;
        };

        $scope.save = function () {
            $scope.isSaving = true;
            if ($scope.userAttributes.id != null) {
                UserAttributes.update($scope.userAttributes, onSaveSuccess, onSaveError);
            } else {
                UserAttributes.save($scope.userAttributes, onSaveSuccess, onSaveError);
            }
        };

        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        $scope.datePickerForExpirationDateofToken = {};

        $scope.datePickerForExpirationDateofToken.status = {
            opened: false
        };

        $scope.datePickerForExpirationDateofTokenOpen = function($event) {
            $scope.datePickerForExpirationDateofToken.status.opened = true;
        };
        $scope.datePickerForCreatedAt = {};

        $scope.datePickerForCreatedAt.status = {
            opened: false
        };

        $scope.datePickerForCreatedAtOpen = function($event) {
            $scope.datePickerForCreatedAt.status.opened = true;
        };
        $scope.datePickerForUpdatedAt = {};

        $scope.datePickerForUpdatedAt.status = {
            opened: false
        };

        $scope.datePickerForUpdatedAtOpen = function($event) {
            $scope.datePickerForUpdatedAt.status.opened = true;
        };
}]);
