'use strict';

angular.module('lokavidyaApp')
    .controller('UserAttributesController', function ($scope, $state, UserAttributes, UserAttributesSearch) {

        $scope.userAttributess = [];
        $scope.loadAll = function() {
            UserAttributes.query(function(result) {
               $scope.userAttributess = result;
            });
        };
        $scope.loadAll();


        $scope.search = function () {
            UserAttributesSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.userAttributess = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.userAttributes = {
                requestToken: null,
                accessToken: null,
                expirationDateofToken: null,
                gender: null,
                language: null,
                latentAbilityScore: null,
                createdAt: null,
                updatedAt: null,
                extraBooleanSettings: false,
                id: null
            };
        };
    });
