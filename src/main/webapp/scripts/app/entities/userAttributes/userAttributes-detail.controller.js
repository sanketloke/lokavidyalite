'use strict';

angular.module('lokavidyaApp')
    .controller('UserAttributesDetailController', function ($scope, $rootScope, $stateParams, entity, UserAttributes, Image, Location) {
        $scope.userAttributes = entity;
        $scope.load = function (id) {
            UserAttributes.get({id: id}, function(result) {
                $scope.userAttributes = result;
            });
        };
        var unsubscribe = $rootScope.$on('lokavidyaApp:userAttributesUpdate', function(event, result) {
            $scope.userAttributes = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
