'use strict';

angular.module('lokavidyaApp')
	.controller('UserAttributesDeleteController', function($scope, $uibModalInstance, entity, UserAttributes) {

        $scope.userAttributes = entity;
        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        $scope.confirmDelete = function (id) {
            UserAttributes.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        };

    });
