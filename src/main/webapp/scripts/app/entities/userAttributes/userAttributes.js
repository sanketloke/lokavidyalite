'use strict';

angular.module('lokavidyaApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('userAttributes', {
                parent: 'entity',
                url: '/userAttributess',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'UserAttributess'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/userAttributes/userAttributess.html',
                        controller: 'UserAttributesController'
                    }
                },
                resolve: {
                }
            })
            .state('userAttributes.detail', {
                parent: 'entity',
                url: '/userAttributes/{id}',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'UserAttributes'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/userAttributes/userAttributes-detail.html',
                        controller: 'UserAttributesDetailController'
                    }
                },
                resolve: {
                    entity: ['$stateParams', 'UserAttributes', function($stateParams, UserAttributes) {
                        return UserAttributes.get({id : $stateParams.id});
                    }]
                }
            })
            .state('userAttributes.new', {
                parent: 'userAttributes',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/userAttributes/userAttributes-dialog.html',
                        controller: 'UserAttributesDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {
                                    requestToken: null,
                                    accessToken: null,
                                    expirationDateofToken: null,
                                    gender: null,
                                    language: null,
                                    latentAbilityScore: null,
                                    createdAt: null,
                                    updatedAt: null,
                                    extraBooleanSettings: false,
                                    id: null
                                };
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('userAttributes', null, { reload: true });
                    }, function() {
                        $state.go('userAttributes');
                    })
                }]
            })
            .state('userAttributes.edit', {
                parent: 'userAttributes',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/userAttributes/userAttributes-dialog.html',
                        controller: 'UserAttributesDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['UserAttributes', function(UserAttributes) {
                                return UserAttributes.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('userAttributes', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            })
            .state('userAttributes.delete', {
                parent: 'userAttributes',
                url: '/{id}/delete',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/userAttributes/userAttributes-delete-dialog.html',
                        controller: 'UserAttributesDeleteController',
                        size: 'md',
                        resolve: {
                            entity: ['UserAttributes', function(UserAttributes) {
                                return UserAttributes.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('userAttributes', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });
