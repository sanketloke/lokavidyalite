'use strict';

angular.module('lokavidyaApp').controller('TutorialDialogController',
    ['$scope', '$stateParams', '$uibModalInstance', '$q', 'entity', 'Tutorial', 'Playlist', 'User', 'ExternalVideo', 'SegmentVideo', 'Tag', 'Lvcategory', 'Comment',
        function($scope, $stateParams, $uibModalInstance, $q, entity, Tutorial, Playlist, User, ExternalVideo, SegmentVideo, Tag, Lvcategory, Comment) {

        $scope.tutorial = entity;
        $scope.playlists = Playlist.query();
        $scope.users = User.query();
        $scope.externalvideos = ExternalVideo.query({filter: 'parenttutorial-is-null'});
        $q.all([$scope.parentTutorial.$promise, $scope.externalvideos.$promise]).then(function() {
            if (!$scope.parentTutorial.externalVideo || !$scope.parentTutorial.externalVideo.id) {
                return $q.reject();
            }
            return ExternalVideo.get({id : $scope.parentTutorial.externalVideo.id}).$promise;
        }).then(function(externalVideo) {
            $scope.externalvideos.push(externalVideo);
        });
        $scope.segmentvideos = SegmentVideo.query({filter: 'parenttutorial-is-null'});
        $q.all([$scope.parentTutorial.$promise, $scope.segmentvideos.$promise]).then(function() {
            if (!$scope.parentTutorial.segmentVideo || !$scope.parentTutorial.segmentVideo.id) {
                return $q.reject();
            }
            return SegmentVideo.get({id : $scope.parentTutorial.segmentVideo.id}).$promise;
        }).then(function(segmentVideo) {
            $scope.segmentvideos.push(segmentVideo);
        });
        $scope.tags = Tag.query();
        $scope.lvcategorys = Lvcategory.query({filter: 'tutorial-is-null'});
        $q.all([$scope.tutorial.$promise, $scope.lvcategorys.$promise]).then(function() {
            if (!$scope.tutorial.lvcategory || !$scope.tutorial.lvcategory.id) {
                return $q.reject();
            }
            return Lvcategory.get({id : $scope.tutorial.lvcategory.id}).$promise;
        }).then(function(lvcategory) {
            $scope.lvcategorys.push(lvcategory);
        });
        $scope.comments = Comment.query();
        $scope.load = function(id) {
            Tutorial.get({id : id}, function(result) {
                $scope.tutorial = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('lokavidyaApp:tutorialUpdate', result);
            $uibModalInstance.close(result);
            $scope.isSaving = false;
        };

        var onSaveError = function (result) {
            $scope.isSaving = false;
        };

        $scope.save = function () {
            $scope.isSaving = true;
            if ($scope.tutorial.id != null) {
                Tutorial.update($scope.tutorial, onSaveSuccess, onSaveError);
            } else {
                Tutorial.save($scope.tutorial, onSaveSuccess, onSaveError);
            }
        };

        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        $scope.datePickerForCreatedAt = {};

        $scope.datePickerForCreatedAt.status = {
            opened: false
        };

        $scope.datePickerForCreatedAtOpen = function($event) {
            $scope.datePickerForCreatedAt.status.opened = true;
        };
        $scope.datePickerForUpdatedAt = {};

        $scope.datePickerForUpdatedAt.status = {
            opened: false
        };

        $scope.datePickerForUpdatedAtOpen = function($event) {
            $scope.datePickerForUpdatedAt.status.opened = true;
        };
}]);
