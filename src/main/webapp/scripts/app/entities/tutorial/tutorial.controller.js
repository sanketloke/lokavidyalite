'use strict';

angular.module('lokavidyaApp')
    .controller('TutorialController', function ($scope, $state, Tutorial, TutorialSearch) {

        $scope.tutorials = [];
        $scope.loadAll = function() {
            Tutorial.query(function(result) {
               $scope.tutorials = result;
            });
        };
        $scope.loadAll();


        $scope.search = function () {
            TutorialSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.tutorials = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.tutorial = {
                name: null,
                description: null,
                upvoteCount: null,
                downvoteCount: null,
                createdAt: null,
                updatedAt: null,
                authorName: null,
                videoUrl: null,
                id: null
            };
        };
    });
