'use strict';

angular.module('lokavidyaApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('tutorial', {
                parent: 'entity',
                url: '/tutorials',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'Tutorials'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/tutorial/tutorials.html',
                        controller: 'TutorialController'
                    }
                },
                resolve: {
                }
            })
            .state('tutorial.detail', {
                parent: 'entity',
                url: '/tutorial/{id}',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'Tutorial'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/tutorial/tutorial-detail.html',
                        controller: 'TutorialDetailController'
                    }
                },
                resolve: {
                    entity: ['$stateParams', 'Tutorial', function($stateParams, Tutorial) {
                        return Tutorial.get({id : $stateParams.id});
                    }]
                }
            })
            .state('tutorial.new', {
                parent: 'tutorial',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/tutorial/tutorial-dialog.html',
                        controller: 'TutorialDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {
                                    name: null,
                                    description: null,
                                    upvoteCount: null,
                                    downvoteCount: null,
                                    createdAt: null,
                                    updatedAt: null,
                                    authorName: null,
                                    videoUrl: null,
                                    id: null
                                };
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('tutorial', null, { reload: true });
                    }, function() {
                        $state.go('tutorial');
                    })
                }]
            })
            .state('tutorial.edit', {
                parent: 'tutorial',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/tutorial/tutorial-dialog.html',
                        controller: 'TutorialDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['Tutorial', function(Tutorial) {
                                return Tutorial.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('tutorial', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            })
            .state('tutorial.delete', {
                parent: 'tutorial',
                url: '/{id}/delete',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/tutorial/tutorial-delete-dialog.html',
                        controller: 'TutorialDeleteController',
                        size: 'md',
                        resolve: {
                            entity: ['Tutorial', function(Tutorial) {
                                return Tutorial.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('tutorial', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });
