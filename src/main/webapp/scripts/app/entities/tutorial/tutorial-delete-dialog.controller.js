'use strict';

angular.module('lokavidyaApp')
	.controller('TutorialDeleteController', function($scope, $uibModalInstance, entity, Tutorial) {

        $scope.tutorial = entity;
        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        $scope.confirmDelete = function (id) {
            Tutorial.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        };

    });
