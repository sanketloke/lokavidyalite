'use strict';

angular.module('lokavidyaApp').controller('CommentDialogController',
    ['$scope', '$stateParams', '$uibModalInstance', '$q', 'entity', 'Comment', 'Tutorial', 'User',
        function($scope, $stateParams, $uibModalInstance, $q, entity, Comment, Tutorial, User) {

        $scope.comment = entity;
        $scope.tutorials = Tutorial.query();
        $scope.users = User.query();
        $scope.load = function(id) {
            Comment.get({id : id}, function(result) {
                $scope.comment = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('lokavidyaApp:commentUpdate', result);
            $uibModalInstance.close(result);
            $scope.isSaving = false;
        };

        var onSaveError = function (result) {
            $scope.isSaving = false;
        };

        $scope.save = function () {
            $scope.isSaving = true;
            if ($scope.comment.id != null) {
                Comment.update($scope.comment, onSaveSuccess, onSaveError);
            } else {
                Comment.save($scope.comment, onSaveSuccess, onSaveError);
            }
        };

        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
}]);
