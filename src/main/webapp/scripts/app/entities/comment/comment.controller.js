'use strict';

angular.module('lokavidyaApp')
    .controller('CommentController', function ($scope, $state, Comment, CommentSearch) {

        $scope.comments = [];
        $scope.loadAll = function() {
            Comment.query(function(result) {
               $scope.comments = result;
            });
        };
        $scope.loadAll();


        $scope.search = function () {
            CommentSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.comments = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.comment = {
                commentContent: null,
                id: null
            };
        };
    });
