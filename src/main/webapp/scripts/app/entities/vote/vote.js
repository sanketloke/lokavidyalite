'use strict';

angular.module('lokavidyaApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('vote', {
                parent: 'entity',
                url: '/votes',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'Votes'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/vote/votes.html',
                        controller: 'VoteController'
                    }
                },
                resolve: {
                }
            })
            .state('vote.detail', {
                parent: 'entity',
                url: '/vote/{id}',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'Vote'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/vote/vote-detail.html',
                        controller: 'VoteDetailController'
                    }
                },
                resolve: {
                    entity: ['$stateParams', 'Vote', function($stateParams, Vote) {
                        return Vote.get({id : $stateParams.id});
                    }]
                }
            })
            .state('vote.new', {
                parent: 'vote',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/vote/vote-dialog.html',
                        controller: 'VoteDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {
                                    type: false,
                                    id: null
                                };
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('vote', null, { reload: true });
                    }, function() {
                        $state.go('vote');
                    })
                }]
            })
            .state('vote.edit', {
                parent: 'vote',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/vote/vote-dialog.html',
                        controller: 'VoteDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['Vote', function(Vote) {
                                return Vote.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('vote', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            })
            .state('vote.delete', {
                parent: 'vote',
                url: '/{id}/delete',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/vote/vote-delete-dialog.html',
                        controller: 'VoteDeleteController',
                        size: 'md',
                        resolve: {
                            entity: ['Vote', function(Vote) {
                                return Vote.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('vote', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });
