'use strict';

angular.module('lokavidyaApp')
    .controller('VoteController', function ($scope, $state, Vote, VoteSearch) {

        $scope.votes = [];
        $scope.loadAll = function() {
            Vote.query(function(result) {
               $scope.votes = result;
            });
        };
        $scope.loadAll();


        $scope.search = function () {
            VoteSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.votes = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.vote = {
                type: false,
                id: null
            };
        };
    });
