'use strict';

angular.module('lokavidyaApp').controller('VoteDialogController',
    ['$scope', '$stateParams', '$uibModalInstance', 'entity', 'Vote',
        function($scope, $stateParams, $uibModalInstance, entity, Vote) {

        $scope.vote = entity;
        $scope.load = function(id) {
            Vote.get({id : id}, function(result) {
                $scope.vote = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('lokavidyaApp:voteUpdate', result);
            $uibModalInstance.close(result);
            $scope.isSaving = false;
        };

        var onSaveError = function (result) {
            $scope.isSaving = false;
        };

        $scope.save = function () {
            $scope.isSaving = true;
            if ($scope.vote.id != null) {
                Vote.update($scope.vote, onSaveSuccess, onSaveError);
            } else {
                Vote.save($scope.vote, onSaveSuccess, onSaveError);
            }
        };

        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
}]);
