'use strict';

angular.module('lokavidyaApp')
	.controller('VoteDeleteController', function($scope, $uibModalInstance, entity, Vote) {

        $scope.vote = entity;
        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        $scope.confirmDelete = function (id) {
            Vote.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        };

    });
