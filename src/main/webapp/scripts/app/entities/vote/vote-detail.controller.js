'use strict';

angular.module('lokavidyaApp')
    .controller('VoteDetailController', function ($scope, $rootScope, $stateParams, entity, Vote) {
        $scope.vote = entity;
        $scope.load = function (id) {
            Vote.get({id: id}, function(result) {
                $scope.vote = result;
            });
        };
        var unsubscribe = $rootScope.$on('lokavidyaApp:voteUpdate', function(event, result) {
            $scope.vote = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
