'use strict';

angular.module('lokavidyaApp')
	.controller('SegmentVideoDeleteController', function($scope, $uibModalInstance, entity, SegmentVideo) {

        $scope.segmentVideo = entity;
        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        $scope.confirmDelete = function (id) {
            SegmentVideo.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        };

    });
