'use strict';

angular.module('lokavidyaApp')
    .controller('SegmentVideoController', function ($scope, $state, SegmentVideo, SegmentVideoSearch) {

        $scope.segmentVideos = [];
        $scope.loadAll = function() {
            SegmentVideo.query(function(result) {
               $scope.segmentVideos = result;
            });
        };
        $scope.loadAll();


        $scope.search = function () {
            SegmentVideoSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.segmentVideos = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.segmentVideo = {
                zipurl: null,
                createdAt: null,
                updatedAt: null,
                sync: false,
                id: null
            };
        };
    });
