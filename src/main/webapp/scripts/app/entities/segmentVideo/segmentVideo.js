'use strict';

angular.module('lokavidyaApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('segmentVideo', {
                parent: 'entity',
                url: '/segmentVideos',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'SegmentVideos'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/segmentVideo/segmentVideos.html',
                        controller: 'SegmentVideoController'
                    }
                },
                resolve: {
                }
            })
            .state('segmentVideo.detail', {
                parent: 'entity',
                url: '/segmentVideo/{id}',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'SegmentVideo'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/segmentVideo/segmentVideo-detail.html',
                        controller: 'SegmentVideoDetailController'
                    }
                },
                resolve: {
                    entity: ['$stateParams', 'SegmentVideo', function($stateParams, SegmentVideo) {
                        return SegmentVideo.get({id : $stateParams.id});
                    }]
                }
            })
            .state('segmentVideo.new', {
                parent: 'segmentVideo',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/segmentVideo/segmentVideo-dialog.html',
                        controller: 'SegmentVideoDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {
                                    zipurl: null,
                                    createdAt: null,
                                    updatedAt: null,
                                    sync: false,
                                    id: null
                                };
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('segmentVideo', null, { reload: true });
                    }, function() {
                        $state.go('segmentVideo');
                    })
                }]
            })
            .state('segmentVideo.edit', {
                parent: 'segmentVideo',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/segmentVideo/segmentVideo-dialog.html',
                        controller: 'SegmentVideoDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['SegmentVideo', function(SegmentVideo) {
                                return SegmentVideo.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('segmentVideo', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            })
            .state('segmentVideo.delete', {
                parent: 'segmentVideo',
                url: '/{id}/delete',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/segmentVideo/segmentVideo-delete-dialog.html',
                        controller: 'SegmentVideoDeleteController',
                        size: 'md',
                        resolve: {
                            entity: ['SegmentVideo', function(SegmentVideo) {
                                return SegmentVideo.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('segmentVideo', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });
