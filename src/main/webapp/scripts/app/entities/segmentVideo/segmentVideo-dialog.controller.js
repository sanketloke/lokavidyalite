'use strict';

angular.module('lokavidyaApp').controller('SegmentVideoDialogController',
    ['$scope', '$stateParams', '$uibModalInstance', 'entity', 'SegmentVideo', 'Tutorial',
        function($scope, $stateParams, $uibModalInstance, entity, SegmentVideo, Tutorial) {

        $scope.segmentVideo = entity;
        $scope.tutorials = Tutorial.query();
        $scope.load = function(id) {
            SegmentVideo.get({id : id}, function(result) {
                $scope.segmentVideo = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('lokavidyaApp:segmentVideoUpdate', result);
            $uibModalInstance.close(result);
            $scope.isSaving = false;
        };

        var onSaveError = function (result) {
            $scope.isSaving = false;
        };

        $scope.save = function () {
            $scope.isSaving = true;
            if ($scope.segmentVideo.id != null) {
                SegmentVideo.update($scope.segmentVideo, onSaveSuccess, onSaveError);
            } else {
                SegmentVideo.save($scope.segmentVideo, onSaveSuccess, onSaveError);
            }
        };

        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        $scope.datePickerForCreatedAt = {};

        $scope.datePickerForCreatedAt.status = {
            opened: false
        };

        $scope.datePickerForCreatedAtOpen = function($event) {
            $scope.datePickerForCreatedAt.status.opened = true;
        };
        $scope.datePickerForUpdatedAt = {};

        $scope.datePickerForUpdatedAt.status = {
            opened: false
        };

        $scope.datePickerForUpdatedAtOpen = function($event) {
            $scope.datePickerForUpdatedAt.status.opened = true;
        };
}]);
