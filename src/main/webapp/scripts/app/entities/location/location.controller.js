'use strict';

angular.module('lokavidyaApp')
    .controller('LocationController', function ($scope, $state, Location, LocationSearch) {

        $scope.locations = [];
        $scope.loadAll = function() {
            Location.query(function(result) {
               $scope.locations = result;
            });
        };
        $scope.loadAll();


        $scope.search = function () {
            LocationSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.locations = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.location = {
                locationName: null,
                latitude: null,
                longitude: null,
                id: null
            };
        };
    });
