'use strict';

angular.module('lokavidyaApp')
    .controller('ExternalVideoController', function ($scope, $state, ExternalVideo, ExternalVideoSearch) {

        $scope.externalVideos = [];
        $scope.loadAll = function() {
            ExternalVideo.query(function(result) {
               $scope.externalVideos = result;
            });
        };
        $scope.loadAll();


        $scope.search = function () {
            ExternalVideoSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.externalVideos = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.externalVideo = {
                title: null,
                url: null,
                isYoutube: false,
                createdAt: null,
                updatedAt: null,
                id: null
            };
        };
    });
