'use strict';

angular.module('lokavidyaApp')
	.controller('ExternalVideoDeleteController', function($scope, $uibModalInstance, entity, ExternalVideo) {

        $scope.externalVideo = entity;
        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        $scope.confirmDelete = function (id) {
            ExternalVideo.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        };

    });
