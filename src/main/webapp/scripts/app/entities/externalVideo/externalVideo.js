'use strict';

angular.module('lokavidyaApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('externalVideo', {
                parent: 'entity',
                url: '/externalVideos',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'ExternalVideos'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/externalVideo/externalVideos.html',
                        controller: 'ExternalVideoController'
                    }
                },
                resolve: {
                }
            })
            .state('externalVideo.detail', {
                parent: 'entity',
                url: '/externalVideo/{id}',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'ExternalVideo'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/externalVideo/externalVideo-detail.html',
                        controller: 'ExternalVideoDetailController'
                    }
                },
                resolve: {
                    entity: ['$stateParams', 'ExternalVideo', function($stateParams, ExternalVideo) {
                        return ExternalVideo.get({id : $stateParams.id});
                    }]
                }
            })
            .state('externalVideo.new', {
                parent: 'externalVideo',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/externalVideo/externalVideo-dialog.html',
                        controller: 'ExternalVideoDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {
                                    title: null,
                                    url: null,
                                    isYoutube: false,
                                    createdAt: null,
                                    updatedAt: null,
                                    id: null
                                };
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('externalVideo', null, { reload: true });
                    }, function() {
                        $state.go('externalVideo');
                    })
                }]
            })
            .state('externalVideo.edit', {
                parent: 'externalVideo',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/externalVideo/externalVideo-dialog.html',
                        controller: 'ExternalVideoDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['ExternalVideo', function(ExternalVideo) {
                                return ExternalVideo.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('externalVideo', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            })
            .state('externalVideo.delete', {
                parent: 'externalVideo',
                url: '/{id}/delete',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'scripts/app/entities/externalVideo/externalVideo-delete-dialog.html',
                        controller: 'ExternalVideoDeleteController',
                        size: 'md',
                        resolve: {
                            entity: ['ExternalVideo', function(ExternalVideo) {
                                return ExternalVideo.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('externalVideo', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });
