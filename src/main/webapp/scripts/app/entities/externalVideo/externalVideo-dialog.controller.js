'use strict';

angular.module('lokavidyaApp').controller('ExternalVideoDialogController',
    ['$scope', '$stateParams', '$uibModalInstance', 'entity', 'ExternalVideo', 'Tutorial',
        function($scope, $stateParams, $uibModalInstance, entity, ExternalVideo, Tutorial) {

        $scope.externalVideo = entity;
        $scope.tutorials = Tutorial.query();
        $scope.load = function(id) {
            ExternalVideo.get({id : id}, function(result) {
                $scope.externalVideo = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('lokavidyaApp:externalVideoUpdate', result);
            $uibModalInstance.close(result);
            $scope.isSaving = false;
        };

        var onSaveError = function (result) {
            $scope.isSaving = false;
        };

        $scope.save = function () {
            $scope.isSaving = true;
            if ($scope.externalVideo.id != null) {
                ExternalVideo.update($scope.externalVideo, onSaveSuccess, onSaveError);
            } else {
                ExternalVideo.save($scope.externalVideo, onSaveSuccess, onSaveError);
            }
        };

        $scope.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        $scope.datePickerForCreatedAt = {};

        $scope.datePickerForCreatedAt.status = {
            opened: false
        };

        $scope.datePickerForCreatedAtOpen = function($event) {
            $scope.datePickerForCreatedAt.status.opened = true;
        };
        $scope.datePickerForUpdatedAt = {};

        $scope.datePickerForUpdatedAt.status = {
            opened: false
        };

        $scope.datePickerForUpdatedAtOpen = function($event) {
            $scope.datePickerForUpdatedAt.status.opened = true;
        };
}]);
