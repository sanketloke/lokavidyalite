'use strict';

angular.module('lokavidyaApp')
    .controller('ExternalVideoDetailController', function ($scope, $rootScope, $stateParams, entity, ExternalVideo, Tutorial) {
        $scope.externalVideo = entity;
        $scope.load = function (id) {
            ExternalVideo.get({id: id}, function(result) {
                $scope.externalVideo = result;
            });
        };
        var unsubscribe = $rootScope.$on('lokavidyaApp:externalVideoUpdate', function(event, result) {
            $scope.externalVideo = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
