'use strict';

angular.module('lokavidyaApp')
    .controller('ImageController', function ($scope, $state, Image, ImageSearch) {

        $scope.images = [];
        $scope.loadAll = function() {
            Image.query(function(result) {
               $scope.images = result;
            });
        };
        $scope.loadAll();


        $scope.search = function () {
            ImageSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.images = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.image = {
                name: null,
                type: null,
                caption: null,
                url: null,
                sync: false,
                isSlide: false,
                slideXML: null,
                createdAt: null,
                updatedAt: null,
                id: null
            };
        };
    });
