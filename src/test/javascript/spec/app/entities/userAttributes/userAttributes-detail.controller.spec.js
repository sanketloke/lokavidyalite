'use strict';

describe('Controller Tests', function() {

    describe('UserAttributes Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockUserAttributes, MockImage, MockLocation;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockUserAttributes = jasmine.createSpy('MockUserAttributes');
            MockImage = jasmine.createSpy('MockImage');
            MockLocation = jasmine.createSpy('MockLocation');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity ,
                'UserAttributes': MockUserAttributes,
                'Image': MockImage,
                'Location': MockLocation
            };
            createController = function() {
                $injector.get('$controller')("UserAttributesDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'lokavidyaApp:userAttributesUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
