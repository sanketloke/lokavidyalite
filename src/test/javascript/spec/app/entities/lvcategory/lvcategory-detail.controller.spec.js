'use strict';

describe('Controller Tests', function() {

    describe('Lvcategory Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockLvcategory, MockImage;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockLvcategory = jasmine.createSpy('MockLvcategory');
            MockImage = jasmine.createSpy('MockImage');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity ,
                'Lvcategory': MockLvcategory,
                'Image': MockImage
            };
            createController = function() {
                $injector.get('$controller')("LvcategoryDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'lokavidyaApp:lvcategoryUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
