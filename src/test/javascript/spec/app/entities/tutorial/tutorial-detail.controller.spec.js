'use strict';

describe('Controller Tests', function() {

    describe('Tutorial Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockTutorial, MockPlaylist, MockUser, MockExternalVideo, MockSegmentVideo, MockTag, MockLvcategory, MockComment;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockTutorial = jasmine.createSpy('MockTutorial');
            MockPlaylist = jasmine.createSpy('MockPlaylist');
            MockUser = jasmine.createSpy('MockUser');
            MockExternalVideo = jasmine.createSpy('MockExternalVideo');
            MockSegmentVideo = jasmine.createSpy('MockSegmentVideo');
            MockTag = jasmine.createSpy('MockTag');
            MockLvcategory = jasmine.createSpy('MockLvcategory');
            MockComment = jasmine.createSpy('MockComment');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity ,
                'Tutorial': MockTutorial,
                'Playlist': MockPlaylist,
                'User': MockUser,
                'ExternalVideo': MockExternalVideo,
                'SegmentVideo': MockSegmentVideo,
                'Tag': MockTag,
                'Lvcategory': MockLvcategory,
                'Comment': MockComment
            };
            createController = function() {
                $injector.get('$controller')("TutorialDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'lokavidyaApp:tutorialUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
