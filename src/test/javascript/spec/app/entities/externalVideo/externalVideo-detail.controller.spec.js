'use strict';

describe('Controller Tests', function() {

    describe('ExternalVideo Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockExternalVideo, MockTutorial;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockExternalVideo = jasmine.createSpy('MockExternalVideo');
            MockTutorial = jasmine.createSpy('MockTutorial');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity ,
                'ExternalVideo': MockExternalVideo,
                'Tutorial': MockTutorial
            };
            createController = function() {
                $injector.get('$controller')("ExternalVideoDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'lokavidyaApp:externalVideoUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
