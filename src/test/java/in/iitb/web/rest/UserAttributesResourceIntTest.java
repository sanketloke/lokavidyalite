package in.iitb.web.rest;

import in.iitb.Application;
import in.iitb.domain.UserAttributes;
import in.iitb.repository.UserAttributesRepository;
import in.iitb.repository.search.UserAttributesSearchRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import in.iitb.domain.enumeration.Gender;
import in.iitb.domain.enumeration.Language;

/**
 * Test class for the UserAttributesResource REST controller.
 *
 * @see UserAttributesResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class UserAttributesResourceIntTest {

    private static final String DEFAULT_REQUEST_TOKEN = "AAAAA";
    private static final String UPDATED_REQUEST_TOKEN = "BBBBB";
    private static final String DEFAULT_ACCESS_TOKEN = "AAAAA";
    private static final String UPDATED_ACCESS_TOKEN = "BBBBB";

    private static final LocalDate DEFAULT_EXPIRATION_DATEOF_TOKEN = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_EXPIRATION_DATEOF_TOKEN = LocalDate.now(ZoneId.systemDefault());


    private static final Gender DEFAULT_GENDER = Gender.MALE;
    private static final Gender UPDATED_GENDER = Gender.FEMALE;


    private static final Language DEFAULT_LANGUAGE = Language.ENGLISH;
    private static final Language UPDATED_LANGUAGE = Language.HINDI;

    private static final Double DEFAULT_LATENT_ABILITY_SCORE = 1D;
    private static final Double UPDATED_LATENT_ABILITY_SCORE = 2D;

    private static final LocalDate DEFAULT_CREATED_AT = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATED_AT = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_UPDATED_AT = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_UPDATED_AT = LocalDate.now(ZoneId.systemDefault());

    private static final Boolean DEFAULT_EXTRA_BOOLEAN_SETTINGS = false;
    private static final Boolean UPDATED_EXTRA_BOOLEAN_SETTINGS = true;

    @Inject
    private UserAttributesRepository userAttributesRepository;

    @Inject
    private UserAttributesSearchRepository userAttributesSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restUserAttributesMockMvc;

    private UserAttributes userAttributes;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        UserAttributesResource userAttributesResource = new UserAttributesResource();
        ReflectionTestUtils.setField(userAttributesResource, "userAttributesSearchRepository", userAttributesSearchRepository);
        ReflectionTestUtils.setField(userAttributesResource, "userAttributesRepository", userAttributesRepository);
        this.restUserAttributesMockMvc = MockMvcBuilders.standaloneSetup(userAttributesResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        userAttributes = new UserAttributes();
        userAttributes.setRequestToken(DEFAULT_REQUEST_TOKEN);
        userAttributes.setAccessToken(DEFAULT_ACCESS_TOKEN);
        userAttributes.setExpirationDateofToken(DEFAULT_EXPIRATION_DATEOF_TOKEN);
        userAttributes.setGender(DEFAULT_GENDER);
        userAttributes.setLanguage(DEFAULT_LANGUAGE);
        userAttributes.setLatentAbilityScore(DEFAULT_LATENT_ABILITY_SCORE);
        userAttributes.setCreatedAt(DEFAULT_CREATED_AT);
        userAttributes.setUpdatedAt(DEFAULT_UPDATED_AT);
        userAttributes.setExtraBooleanSettings(DEFAULT_EXTRA_BOOLEAN_SETTINGS);
    }

    @Test
    @Transactional
    public void createUserAttributes() throws Exception {
        int databaseSizeBeforeCreate = userAttributesRepository.findAll().size();

        // Create the UserAttributes

        restUserAttributesMockMvc.perform(post("/api/userAttributess")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(userAttributes)))
                .andExpect(status().isCreated());

        // Validate the UserAttributes in the database
        List<UserAttributes> userAttributess = userAttributesRepository.findAll();
        assertThat(userAttributess).hasSize(databaseSizeBeforeCreate + 1);
        UserAttributes testUserAttributes = userAttributess.get(userAttributess.size() - 1);
        assertThat(testUserAttributes.getRequestToken()).isEqualTo(DEFAULT_REQUEST_TOKEN);
        assertThat(testUserAttributes.getAccessToken()).isEqualTo(DEFAULT_ACCESS_TOKEN);
        assertThat(testUserAttributes.getExpirationDateofToken()).isEqualTo(DEFAULT_EXPIRATION_DATEOF_TOKEN);
        assertThat(testUserAttributes.getGender()).isEqualTo(DEFAULT_GENDER);
        assertThat(testUserAttributes.getLanguage()).isEqualTo(DEFAULT_LANGUAGE);
        assertThat(testUserAttributes.getLatentAbilityScore()).isEqualTo(DEFAULT_LATENT_ABILITY_SCORE);
        assertThat(testUserAttributes.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testUserAttributes.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
        assertThat(testUserAttributes.getExtraBooleanSettings()).isEqualTo(DEFAULT_EXTRA_BOOLEAN_SETTINGS);
    }

    @Test
    @Transactional
    public void checkExtraBooleanSettingsIsRequired() throws Exception {
        int databaseSizeBeforeTest = userAttributesRepository.findAll().size();
        // set the field null
        userAttributes.setExtraBooleanSettings(null);

        // Create the UserAttributes, which fails.

        restUserAttributesMockMvc.perform(post("/api/userAttributess")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(userAttributes)))
                .andExpect(status().isBadRequest());

        List<UserAttributes> userAttributess = userAttributesRepository.findAll();
        assertThat(userAttributess).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllUserAttributess() throws Exception {
        // Initialize the database
        userAttributesRepository.saveAndFlush(userAttributes);

        // Get all the userAttributess
        restUserAttributesMockMvc.perform(get("/api/userAttributess?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(userAttributes.getId().intValue())))
                .andExpect(jsonPath("$.[*].requestToken").value(hasItem(DEFAULT_REQUEST_TOKEN.toString())))
                .andExpect(jsonPath("$.[*].accessToken").value(hasItem(DEFAULT_ACCESS_TOKEN.toString())))
                .andExpect(jsonPath("$.[*].expirationDateofToken").value(hasItem(DEFAULT_EXPIRATION_DATEOF_TOKEN.toString())))
                .andExpect(jsonPath("$.[*].gender").value(hasItem(DEFAULT_GENDER.toString())))
                .andExpect(jsonPath("$.[*].language").value(hasItem(DEFAULT_LANGUAGE.toString())))
                .andExpect(jsonPath("$.[*].latentAbilityScore").value(hasItem(DEFAULT_LATENT_ABILITY_SCORE.doubleValue())))
                .andExpect(jsonPath("$.[*].createdAt").value(hasItem(DEFAULT_CREATED_AT.toString())))
                .andExpect(jsonPath("$.[*].updatedAt").value(hasItem(DEFAULT_UPDATED_AT.toString())))
                .andExpect(jsonPath("$.[*].extraBooleanSettings").value(hasItem(DEFAULT_EXTRA_BOOLEAN_SETTINGS.booleanValue())));
    }

    @Test
    @Transactional
    public void getUserAttributes() throws Exception {
        // Initialize the database
        userAttributesRepository.saveAndFlush(userAttributes);

        // Get the userAttributes
        restUserAttributesMockMvc.perform(get("/api/userAttributess/{id}", userAttributes.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(userAttributes.getId().intValue()))
            .andExpect(jsonPath("$.requestToken").value(DEFAULT_REQUEST_TOKEN.toString()))
            .andExpect(jsonPath("$.accessToken").value(DEFAULT_ACCESS_TOKEN.toString()))
            .andExpect(jsonPath("$.expirationDateofToken").value(DEFAULT_EXPIRATION_DATEOF_TOKEN.toString()))
            .andExpect(jsonPath("$.gender").value(DEFAULT_GENDER.toString()))
            .andExpect(jsonPath("$.language").value(DEFAULT_LANGUAGE.toString()))
            .andExpect(jsonPath("$.latentAbilityScore").value(DEFAULT_LATENT_ABILITY_SCORE.doubleValue()))
            .andExpect(jsonPath("$.createdAt").value(DEFAULT_CREATED_AT.toString()))
            .andExpect(jsonPath("$.updatedAt").value(DEFAULT_UPDATED_AT.toString()))
            .andExpect(jsonPath("$.extraBooleanSettings").value(DEFAULT_EXTRA_BOOLEAN_SETTINGS.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingUserAttributes() throws Exception {
        // Get the userAttributes
        restUserAttributesMockMvc.perform(get("/api/userAttributess/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateUserAttributes() throws Exception {
        // Initialize the database
        userAttributesRepository.saveAndFlush(userAttributes);

		int databaseSizeBeforeUpdate = userAttributesRepository.findAll().size();

        // Update the userAttributes
        userAttributes.setRequestToken(UPDATED_REQUEST_TOKEN);
        userAttributes.setAccessToken(UPDATED_ACCESS_TOKEN);
        userAttributes.setExpirationDateofToken(UPDATED_EXPIRATION_DATEOF_TOKEN);
        userAttributes.setGender(UPDATED_GENDER);
        userAttributes.setLanguage(UPDATED_LANGUAGE);
        userAttributes.setLatentAbilityScore(UPDATED_LATENT_ABILITY_SCORE);
        userAttributes.setCreatedAt(UPDATED_CREATED_AT);
        userAttributes.setUpdatedAt(UPDATED_UPDATED_AT);
        userAttributes.setExtraBooleanSettings(UPDATED_EXTRA_BOOLEAN_SETTINGS);

        restUserAttributesMockMvc.perform(put("/api/userAttributess")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(userAttributes)))
                .andExpect(status().isOk());

        // Validate the UserAttributes in the database
        List<UserAttributes> userAttributess = userAttributesRepository.findAll();
        assertThat(userAttributess).hasSize(databaseSizeBeforeUpdate);
        UserAttributes testUserAttributes = userAttributess.get(userAttributess.size() - 1);
        assertThat(testUserAttributes.getRequestToken()).isEqualTo(UPDATED_REQUEST_TOKEN);
        assertThat(testUserAttributes.getAccessToken()).isEqualTo(UPDATED_ACCESS_TOKEN);
        assertThat(testUserAttributes.getExpirationDateofToken()).isEqualTo(UPDATED_EXPIRATION_DATEOF_TOKEN);
        assertThat(testUserAttributes.getGender()).isEqualTo(UPDATED_GENDER);
        assertThat(testUserAttributes.getLanguage()).isEqualTo(UPDATED_LANGUAGE);
        assertThat(testUserAttributes.getLatentAbilityScore()).isEqualTo(UPDATED_LATENT_ABILITY_SCORE);
        assertThat(testUserAttributes.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testUserAttributes.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);
        assertThat(testUserAttributes.getExtraBooleanSettings()).isEqualTo(UPDATED_EXTRA_BOOLEAN_SETTINGS);
    }

    @Test
    @Transactional
    public void deleteUserAttributes() throws Exception {
        // Initialize the database
        userAttributesRepository.saveAndFlush(userAttributes);

		int databaseSizeBeforeDelete = userAttributesRepository.findAll().size();

        // Get the userAttributes
        restUserAttributesMockMvc.perform(delete("/api/userAttributess/{id}", userAttributes.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<UserAttributes> userAttributess = userAttributesRepository.findAll();
        assertThat(userAttributess).hasSize(databaseSizeBeforeDelete - 1);
    }
}
