package in.iitb.web.rest;

import in.iitb.Application;
import in.iitb.domain.Tutorial;
import in.iitb.repository.TutorialRepository;
import in.iitb.repository.search.TutorialSearchRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the TutorialResource REST controller.
 *
 * @see TutorialResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class TutorialResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAA";
    private static final String UPDATED_NAME = "BBBBB";
    private static final String DEFAULT_DESCRIPTION = "AAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBB";

    private static final Long DEFAULT_UPVOTE_COUNT = 1L;
    private static final Long UPDATED_UPVOTE_COUNT = 2L;

    private static final Long DEFAULT_DOWNVOTE_COUNT = 1L;
    private static final Long UPDATED_DOWNVOTE_COUNT = 2L;

    private static final LocalDate DEFAULT_CREATED_AT = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATED_AT = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_UPDATED_AT = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_UPDATED_AT = LocalDate.now(ZoneId.systemDefault());
    private static final String DEFAULT_AUTHOR_NAME = "AAAAA";
    private static final String UPDATED_AUTHOR_NAME = "BBBBB";
    private static final String DEFAULT_VIDEO_URL = "AAAAA";
    private static final String UPDATED_VIDEO_URL = "BBBBB";

    @Inject
    private TutorialRepository tutorialRepository;

    @Inject
    private TutorialSearchRepository tutorialSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restTutorialMockMvc;

    private Tutorial tutorial;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        TutorialResource tutorialResource = new TutorialResource();
        ReflectionTestUtils.setField(tutorialResource, "tutorialSearchRepository", tutorialSearchRepository);
        ReflectionTestUtils.setField(tutorialResource, "tutorialRepository", tutorialRepository);
        this.restTutorialMockMvc = MockMvcBuilders.standaloneSetup(tutorialResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        tutorial = new Tutorial();
        tutorial.setName(DEFAULT_NAME);
        tutorial.setDescription(DEFAULT_DESCRIPTION);
        tutorial.setUpvoteCount(DEFAULT_UPVOTE_COUNT);
        tutorial.setDownvoteCount(DEFAULT_DOWNVOTE_COUNT);
        tutorial.setCreatedAt(DEFAULT_CREATED_AT);
        tutorial.setUpdatedAt(DEFAULT_UPDATED_AT);
        tutorial.setAuthorName(DEFAULT_AUTHOR_NAME);
        tutorial.setVideoUrl(DEFAULT_VIDEO_URL);
    }

    @Test
    @Transactional
    public void createTutorial() throws Exception {
        int databaseSizeBeforeCreate = tutorialRepository.findAll().size();

        // Create the Tutorial

        restTutorialMockMvc.perform(post("/api/tutorials")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(tutorial)))
                .andExpect(status().isCreated());

        // Validate the Tutorial in the database
        List<Tutorial> tutorials = tutorialRepository.findAll();
        assertThat(tutorials).hasSize(databaseSizeBeforeCreate + 1);
        Tutorial testTutorial = tutorials.get(tutorials.size() - 1);
        assertThat(testTutorial.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testTutorial.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testTutorial.getUpvoteCount()).isEqualTo(DEFAULT_UPVOTE_COUNT);
        assertThat(testTutorial.getDownvoteCount()).isEqualTo(DEFAULT_DOWNVOTE_COUNT);
        assertThat(testTutorial.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testTutorial.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
        assertThat(testTutorial.getAuthorName()).isEqualTo(DEFAULT_AUTHOR_NAME);
        assertThat(testTutorial.getVideoUrl()).isEqualTo(DEFAULT_VIDEO_URL);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = tutorialRepository.findAll().size();
        // set the field null
        tutorial.setName(null);

        // Create the Tutorial, which fails.

        restTutorialMockMvc.perform(post("/api/tutorials")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(tutorial)))
                .andExpect(status().isBadRequest());

        List<Tutorial> tutorials = tutorialRepository.findAll();
        assertThat(tutorials).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDescriptionIsRequired() throws Exception {
        int databaseSizeBeforeTest = tutorialRepository.findAll().size();
        // set the field null
        tutorial.setDescription(null);

        // Create the Tutorial, which fails.

        restTutorialMockMvc.perform(post("/api/tutorials")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(tutorial)))
                .andExpect(status().isBadRequest());

        List<Tutorial> tutorials = tutorialRepository.findAll();
        assertThat(tutorials).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkVideoUrlIsRequired() throws Exception {
        int databaseSizeBeforeTest = tutorialRepository.findAll().size();
        // set the field null
        tutorial.setVideoUrl(null);

        // Create the Tutorial, which fails.

        restTutorialMockMvc.perform(post("/api/tutorials")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(tutorial)))
                .andExpect(status().isBadRequest());

        List<Tutorial> tutorials = tutorialRepository.findAll();
        assertThat(tutorials).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllTutorials() throws Exception {
        // Initialize the database
        tutorialRepository.saveAndFlush(tutorial);

        // Get all the tutorials
        restTutorialMockMvc.perform(get("/api/tutorials?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(tutorial.getId().intValue())))
                .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
                .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
                .andExpect(jsonPath("$.[*].upvoteCount").value(hasItem(DEFAULT_UPVOTE_COUNT.intValue())))
                .andExpect(jsonPath("$.[*].downvoteCount").value(hasItem(DEFAULT_DOWNVOTE_COUNT.intValue())))
                .andExpect(jsonPath("$.[*].createdAt").value(hasItem(DEFAULT_CREATED_AT.toString())))
                .andExpect(jsonPath("$.[*].updatedAt").value(hasItem(DEFAULT_UPDATED_AT.toString())))
                .andExpect(jsonPath("$.[*].authorName").value(hasItem(DEFAULT_AUTHOR_NAME.toString())))
                .andExpect(jsonPath("$.[*].videoUrl").value(hasItem(DEFAULT_VIDEO_URL.toString())));
    }

    @Test
    @Transactional
    public void getTutorial() throws Exception {
        // Initialize the database
        tutorialRepository.saveAndFlush(tutorial);

        // Get the tutorial
        restTutorialMockMvc.perform(get("/api/tutorials/{id}", tutorial.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(tutorial.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.upvoteCount").value(DEFAULT_UPVOTE_COUNT.intValue()))
            .andExpect(jsonPath("$.downvoteCount").value(DEFAULT_DOWNVOTE_COUNT.intValue()))
            .andExpect(jsonPath("$.createdAt").value(DEFAULT_CREATED_AT.toString()))
            .andExpect(jsonPath("$.updatedAt").value(DEFAULT_UPDATED_AT.toString()))
            .andExpect(jsonPath("$.authorName").value(DEFAULT_AUTHOR_NAME.toString()))
            .andExpect(jsonPath("$.videoUrl").value(DEFAULT_VIDEO_URL.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingTutorial() throws Exception {
        // Get the tutorial
        restTutorialMockMvc.perform(get("/api/tutorials/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTutorial() throws Exception {
        // Initialize the database
        tutorialRepository.saveAndFlush(tutorial);

		int databaseSizeBeforeUpdate = tutorialRepository.findAll().size();

        // Update the tutorial
        tutorial.setName(UPDATED_NAME);
        tutorial.setDescription(UPDATED_DESCRIPTION);
        tutorial.setUpvoteCount(UPDATED_UPVOTE_COUNT);
        tutorial.setDownvoteCount(UPDATED_DOWNVOTE_COUNT);
        tutorial.setCreatedAt(UPDATED_CREATED_AT);
        tutorial.setUpdatedAt(UPDATED_UPDATED_AT);
        tutorial.setAuthorName(UPDATED_AUTHOR_NAME);
        tutorial.setVideoUrl(UPDATED_VIDEO_URL);

        restTutorialMockMvc.perform(put("/api/tutorials")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(tutorial)))
                .andExpect(status().isOk());

        // Validate the Tutorial in the database
        List<Tutorial> tutorials = tutorialRepository.findAll();
        assertThat(tutorials).hasSize(databaseSizeBeforeUpdate);
        Tutorial testTutorial = tutorials.get(tutorials.size() - 1);
        assertThat(testTutorial.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testTutorial.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testTutorial.getUpvoteCount()).isEqualTo(UPDATED_UPVOTE_COUNT);
        assertThat(testTutorial.getDownvoteCount()).isEqualTo(UPDATED_DOWNVOTE_COUNT);
        assertThat(testTutorial.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testTutorial.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);
        assertThat(testTutorial.getAuthorName()).isEqualTo(UPDATED_AUTHOR_NAME);
        assertThat(testTutorial.getVideoUrl()).isEqualTo(UPDATED_VIDEO_URL);
    }

    @Test
    @Transactional
    public void deleteTutorial() throws Exception {
        // Initialize the database
        tutorialRepository.saveAndFlush(tutorial);

		int databaseSizeBeforeDelete = tutorialRepository.findAll().size();

        // Get the tutorial
        restTutorialMockMvc.perform(delete("/api/tutorials/{id}", tutorial.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Tutorial> tutorials = tutorialRepository.findAll();
        assertThat(tutorials).hasSize(databaseSizeBeforeDelete - 1);
    }
}
