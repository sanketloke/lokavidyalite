package in.iitb.web.rest;

import in.iitb.Application;
import in.iitb.domain.ExternalVideo;
import in.iitb.repository.ExternalVideoRepository;
import in.iitb.repository.search.ExternalVideoSearchRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the ExternalVideoResource REST controller.
 *
 * @see ExternalVideoResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class ExternalVideoResourceIntTest {

    private static final String DEFAULT_TITLE = "AAAAA";
    private static final String UPDATED_TITLE = "BBBBB";
    private static final String DEFAULT_URL = "AAAAA";
    private static final String UPDATED_URL = "BBBBB";

    private static final Boolean DEFAULT_IS_YOUTUBE = false;
    private static final Boolean UPDATED_IS_YOUTUBE = true;

    private static final LocalDate DEFAULT_CREATED_AT = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATED_AT = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_UPDATED_AT = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_UPDATED_AT = LocalDate.now(ZoneId.systemDefault());

    @Inject
    private ExternalVideoRepository externalVideoRepository;

    @Inject
    private ExternalVideoSearchRepository externalVideoSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restExternalVideoMockMvc;

    private ExternalVideo externalVideo;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ExternalVideoResource externalVideoResource = new ExternalVideoResource();
        ReflectionTestUtils.setField(externalVideoResource, "externalVideoSearchRepository", externalVideoSearchRepository);
        ReflectionTestUtils.setField(externalVideoResource, "externalVideoRepository", externalVideoRepository);
        this.restExternalVideoMockMvc = MockMvcBuilders.standaloneSetup(externalVideoResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        externalVideo = new ExternalVideo();
        externalVideo.setTitle(DEFAULT_TITLE);
        externalVideo.setUrl(DEFAULT_URL);
        externalVideo.setIsYoutube(DEFAULT_IS_YOUTUBE);
        externalVideo.setCreatedAt(DEFAULT_CREATED_AT);
        externalVideo.setUpdatedAt(DEFAULT_UPDATED_AT);
    }

    @Test
    @Transactional
    public void createExternalVideo() throws Exception {
        int databaseSizeBeforeCreate = externalVideoRepository.findAll().size();

        // Create the ExternalVideo

        restExternalVideoMockMvc.perform(post("/api/externalVideos")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(externalVideo)))
                .andExpect(status().isCreated());

        // Validate the ExternalVideo in the database
        List<ExternalVideo> externalVideos = externalVideoRepository.findAll();
        assertThat(externalVideos).hasSize(databaseSizeBeforeCreate + 1);
        ExternalVideo testExternalVideo = externalVideos.get(externalVideos.size() - 1);
        assertThat(testExternalVideo.getTitle()).isEqualTo(DEFAULT_TITLE);
        assertThat(testExternalVideo.getUrl()).isEqualTo(DEFAULT_URL);
        assertThat(testExternalVideo.getIsYoutube()).isEqualTo(DEFAULT_IS_YOUTUBE);
        assertThat(testExternalVideo.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testExternalVideo.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
    }

    @Test
    @Transactional
    public void checkTitleIsRequired() throws Exception {
        int databaseSizeBeforeTest = externalVideoRepository.findAll().size();
        // set the field null
        externalVideo.setTitle(null);

        // Create the ExternalVideo, which fails.

        restExternalVideoMockMvc.perform(post("/api/externalVideos")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(externalVideo)))
                .andExpect(status().isBadRequest());

        List<ExternalVideo> externalVideos = externalVideoRepository.findAll();
        assertThat(externalVideos).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkUrlIsRequired() throws Exception {
        int databaseSizeBeforeTest = externalVideoRepository.findAll().size();
        // set the field null
        externalVideo.setUrl(null);

        // Create the ExternalVideo, which fails.

        restExternalVideoMockMvc.perform(post("/api/externalVideos")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(externalVideo)))
                .andExpect(status().isBadRequest());

        List<ExternalVideo> externalVideos = externalVideoRepository.findAll();
        assertThat(externalVideos).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkIsYoutubeIsRequired() throws Exception {
        int databaseSizeBeforeTest = externalVideoRepository.findAll().size();
        // set the field null
        externalVideo.setIsYoutube(null);

        // Create the ExternalVideo, which fails.

        restExternalVideoMockMvc.perform(post("/api/externalVideos")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(externalVideo)))
                .andExpect(status().isBadRequest());

        List<ExternalVideo> externalVideos = externalVideoRepository.findAll();
        assertThat(externalVideos).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllExternalVideos() throws Exception {
        // Initialize the database
        externalVideoRepository.saveAndFlush(externalVideo);

        // Get all the externalVideos
        restExternalVideoMockMvc.perform(get("/api/externalVideos?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(externalVideo.getId().intValue())))
                .andExpect(jsonPath("$.[*].title").value(hasItem(DEFAULT_TITLE.toString())))
                .andExpect(jsonPath("$.[*].url").value(hasItem(DEFAULT_URL.toString())))
                .andExpect(jsonPath("$.[*].isYoutube").value(hasItem(DEFAULT_IS_YOUTUBE.booleanValue())))
                .andExpect(jsonPath("$.[*].createdAt").value(hasItem(DEFAULT_CREATED_AT.toString())))
                .andExpect(jsonPath("$.[*].updatedAt").value(hasItem(DEFAULT_UPDATED_AT.toString())));
    }

    @Test
    @Transactional
    public void getExternalVideo() throws Exception {
        // Initialize the database
        externalVideoRepository.saveAndFlush(externalVideo);

        // Get the externalVideo
        restExternalVideoMockMvc.perform(get("/api/externalVideos/{id}", externalVideo.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(externalVideo.getId().intValue()))
            .andExpect(jsonPath("$.title").value(DEFAULT_TITLE.toString()))
            .andExpect(jsonPath("$.url").value(DEFAULT_URL.toString()))
            .andExpect(jsonPath("$.isYoutube").value(DEFAULT_IS_YOUTUBE.booleanValue()))
            .andExpect(jsonPath("$.createdAt").value(DEFAULT_CREATED_AT.toString()))
            .andExpect(jsonPath("$.updatedAt").value(DEFAULT_UPDATED_AT.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingExternalVideo() throws Exception {
        // Get the externalVideo
        restExternalVideoMockMvc.perform(get("/api/externalVideos/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateExternalVideo() throws Exception {
        // Initialize the database
        externalVideoRepository.saveAndFlush(externalVideo);

		int databaseSizeBeforeUpdate = externalVideoRepository.findAll().size();

        // Update the externalVideo
        externalVideo.setTitle(UPDATED_TITLE);
        externalVideo.setUrl(UPDATED_URL);
        externalVideo.setIsYoutube(UPDATED_IS_YOUTUBE);
        externalVideo.setCreatedAt(UPDATED_CREATED_AT);
        externalVideo.setUpdatedAt(UPDATED_UPDATED_AT);

        restExternalVideoMockMvc.perform(put("/api/externalVideos")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(externalVideo)))
                .andExpect(status().isOk());

        // Validate the ExternalVideo in the database
        List<ExternalVideo> externalVideos = externalVideoRepository.findAll();
        assertThat(externalVideos).hasSize(databaseSizeBeforeUpdate);
        ExternalVideo testExternalVideo = externalVideos.get(externalVideos.size() - 1);
        assertThat(testExternalVideo.getTitle()).isEqualTo(UPDATED_TITLE);
        assertThat(testExternalVideo.getUrl()).isEqualTo(UPDATED_URL);
        assertThat(testExternalVideo.getIsYoutube()).isEqualTo(UPDATED_IS_YOUTUBE);
        assertThat(testExternalVideo.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testExternalVideo.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);
    }

    @Test
    @Transactional
    public void deleteExternalVideo() throws Exception {
        // Initialize the database
        externalVideoRepository.saveAndFlush(externalVideo);

		int databaseSizeBeforeDelete = externalVideoRepository.findAll().size();

        // Get the externalVideo
        restExternalVideoMockMvc.perform(delete("/api/externalVideos/{id}", externalVideo.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<ExternalVideo> externalVideos = externalVideoRepository.findAll();
        assertThat(externalVideos).hasSize(databaseSizeBeforeDelete - 1);
    }
}
