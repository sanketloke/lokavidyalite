package in.iitb.web.rest;

import in.iitb.Application;
import in.iitb.domain.Lvcategory;
import in.iitb.repository.LvcategoryRepository;
import in.iitb.repository.search.LvcategorySearchRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import in.iitb.domain.enumeration.Origin;

/**
 * Test class for the LvcategoryResource REST controller.
 *
 * @see LvcategoryResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class LvcategoryResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAA";
    private static final String UPDATED_NAME = "BBBBB";
    private static final String DEFAULT_DESCRIPTION = "AAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBB";


    private static final Origin DEFAULT_ORIGIN = Origin.SYSTEM;
    private static final Origin UPDATED_ORIGIN = Origin.HUMAN;

    private static final Long DEFAULT_PARENT_CAT_ID = 1L;
    private static final Long UPDATED_PARENT_CAT_ID = 2L;

    private static final Long DEFAULT_LEVEL_FROM_ROOT = 1L;
    private static final Long UPDATED_LEVEL_FROM_ROOT = 2L;

    @Inject
    private LvcategoryRepository lvcategoryRepository;

    @Inject
    private LvcategorySearchRepository lvcategorySearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restLvcategoryMockMvc;

    private Lvcategory lvcategory;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        LvcategoryResource lvcategoryResource = new LvcategoryResource();
        ReflectionTestUtils.setField(lvcategoryResource, "lvcategorySearchRepository", lvcategorySearchRepository);
        ReflectionTestUtils.setField(lvcategoryResource, "lvcategoryRepository", lvcategoryRepository);
        this.restLvcategoryMockMvc = MockMvcBuilders.standaloneSetup(lvcategoryResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        lvcategory = new Lvcategory();
        lvcategory.setName(DEFAULT_NAME);
        lvcategory.setDescription(DEFAULT_DESCRIPTION);
        lvcategory.setOrigin(DEFAULT_ORIGIN);
        lvcategory.setParentCatId(DEFAULT_PARENT_CAT_ID);
        lvcategory.setLevelFromRoot(DEFAULT_LEVEL_FROM_ROOT);
    }

    @Test
    @Transactional
    public void createLvcategory() throws Exception {
        int databaseSizeBeforeCreate = lvcategoryRepository.findAll().size();

        // Create the Lvcategory

        restLvcategoryMockMvc.perform(post("/api/lvcategorys")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(lvcategory)))
                .andExpect(status().isCreated());

        // Validate the Lvcategory in the database
        List<Lvcategory> lvcategorys = lvcategoryRepository.findAll();
        assertThat(lvcategorys).hasSize(databaseSizeBeforeCreate + 1);
        Lvcategory testLvcategory = lvcategorys.get(lvcategorys.size() - 1);
        assertThat(testLvcategory.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testLvcategory.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testLvcategory.getOrigin()).isEqualTo(DEFAULT_ORIGIN);
        assertThat(testLvcategory.getParentCatId()).isEqualTo(DEFAULT_PARENT_CAT_ID);
        assertThat(testLvcategory.getLevelFromRoot()).isEqualTo(DEFAULT_LEVEL_FROM_ROOT);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = lvcategoryRepository.findAll().size();
        // set the field null
        lvcategory.setName(null);

        // Create the Lvcategory, which fails.

        restLvcategoryMockMvc.perform(post("/api/lvcategorys")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(lvcategory)))
                .andExpect(status().isBadRequest());

        List<Lvcategory> lvcategorys = lvcategoryRepository.findAll();
        assertThat(lvcategorys).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDescriptionIsRequired() throws Exception {
        int databaseSizeBeforeTest = lvcategoryRepository.findAll().size();
        // set the field null
        lvcategory.setDescription(null);

        // Create the Lvcategory, which fails.

        restLvcategoryMockMvc.perform(post("/api/lvcategorys")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(lvcategory)))
                .andExpect(status().isBadRequest());

        List<Lvcategory> lvcategorys = lvcategoryRepository.findAll();
        assertThat(lvcategorys).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllLvcategorys() throws Exception {
        // Initialize the database
        lvcategoryRepository.saveAndFlush(lvcategory);

        // Get all the lvcategorys
        restLvcategoryMockMvc.perform(get("/api/lvcategorys?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(lvcategory.getId().intValue())))
                .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
                .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
                .andExpect(jsonPath("$.[*].origin").value(hasItem(DEFAULT_ORIGIN.toString())))
                .andExpect(jsonPath("$.[*].parentCatId").value(hasItem(DEFAULT_PARENT_CAT_ID.intValue())))
                .andExpect(jsonPath("$.[*].levelFromRoot").value(hasItem(DEFAULT_LEVEL_FROM_ROOT.intValue())));
    }

    @Test
    @Transactional
    public void getLvcategory() throws Exception {
        // Initialize the database
        lvcategoryRepository.saveAndFlush(lvcategory);

        // Get the lvcategory
        restLvcategoryMockMvc.perform(get("/api/lvcategorys/{id}", lvcategory.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(lvcategory.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.origin").value(DEFAULT_ORIGIN.toString()))
            .andExpect(jsonPath("$.parentCatId").value(DEFAULT_PARENT_CAT_ID.intValue()))
            .andExpect(jsonPath("$.levelFromRoot").value(DEFAULT_LEVEL_FROM_ROOT.intValue()));
    }

    @Test
    @Transactional
    public void getNonExistingLvcategory() throws Exception {
        // Get the lvcategory
        restLvcategoryMockMvc.perform(get("/api/lvcategorys/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateLvcategory() throws Exception {
        // Initialize the database
        lvcategoryRepository.saveAndFlush(lvcategory);

		int databaseSizeBeforeUpdate = lvcategoryRepository.findAll().size();

        // Update the lvcategory
        lvcategory.setName(UPDATED_NAME);
        lvcategory.setDescription(UPDATED_DESCRIPTION);
        lvcategory.setOrigin(UPDATED_ORIGIN);
        lvcategory.setParentCatId(UPDATED_PARENT_CAT_ID);
        lvcategory.setLevelFromRoot(UPDATED_LEVEL_FROM_ROOT);

        restLvcategoryMockMvc.perform(put("/api/lvcategorys")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(lvcategory)))
                .andExpect(status().isOk());

        // Validate the Lvcategory in the database
        List<Lvcategory> lvcategorys = lvcategoryRepository.findAll();
        assertThat(lvcategorys).hasSize(databaseSizeBeforeUpdate);
        Lvcategory testLvcategory = lvcategorys.get(lvcategorys.size() - 1);
        assertThat(testLvcategory.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testLvcategory.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testLvcategory.getOrigin()).isEqualTo(UPDATED_ORIGIN);
        assertThat(testLvcategory.getParentCatId()).isEqualTo(UPDATED_PARENT_CAT_ID);
        assertThat(testLvcategory.getLevelFromRoot()).isEqualTo(UPDATED_LEVEL_FROM_ROOT);
    }

    @Test
    @Transactional
    public void deleteLvcategory() throws Exception {
        // Initialize the database
        lvcategoryRepository.saveAndFlush(lvcategory);

		int databaseSizeBeforeDelete = lvcategoryRepository.findAll().size();

        // Get the lvcategory
        restLvcategoryMockMvc.perform(delete("/api/lvcategorys/{id}", lvcategory.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Lvcategory> lvcategorys = lvcategoryRepository.findAll();
        assertThat(lvcategorys).hasSize(databaseSizeBeforeDelete - 1);
    }
}
